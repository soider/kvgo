package server

import (
	"net"

	"bitbucket.org/eithz/kvgo/commands"
	"bitbucket.org/eithz/kvgo/utils"

	log "github.com/Sirupsen/logrus"
)

// Listener handles all connection related stuffo
type Listener struct {
	addr   string
	socket *net.TCPListener
	Done   chan bool
	Error  chan error
}

// Init creates channels
func (server *Listener) init() {
	server.Error = make(chan error)
	server.Done = make(chan bool)
}

// Listen on configured port
func (server *Listener) Listen() {
	log.Info("Start listeting server")
	tcpAddr, err := net.ResolveTCPAddr("tcp", server.addr)
	go server.handleListenError(err)
	socket, err := net.ListenTCP("tcp", tcpAddr)
	go server.handleListenError(err)
	server.socket = socket
}

// Run listener main loop
func (server *Listener) Run() {
	go server.run()
}

//Stop gracefully stop this server and cleanup
// TODO: Send close signal to clients
func (server *Listener) Stop() {
	log.Debug("Stopping server")
	server.Done <- true
}

func (server *Listener) run() {
	for {
		log.Info("Waiting for connection")
		connection, err := server.socket.Accept()
		if err != nil {
			go server.handleAcceptorError(err)
		} else {
			go server.handleConnection(connection)
		}
	}
}

// handleConnection handle accepted client socket
func (server *Listener) handleConnection(connection net.Conn) {
	log.Info("Handling connection")
	for {
		message, err := server.readMessage(connection)
		if err != nil {
			server.handleClientError(err, connection)
			return
		}
		log.WithFields(log.Fields{"message": message}).Debug("Incoming message")
		command := commands.Dispatch(message)
		resultChan := make(chan commands.Result)
		errChan := make(chan error)
		defer close(errChan)
		defer close(resultChan)
		go command(connection, message, resultChan, errChan)

		select {
		case result := <-resultChan:
			if result.Answer != nil {
				connection.Write([]byte(result.Answer))
				connection.Write([]byte("\n"))
			}
			if result.Close {
				connection.Close()
				return
			}
		case err := <-errChan:
			connection.Write([]byte("Error: " + err.Error() + "\n"))
		}
	}
}

func (server *Listener) readMessage(connection net.Conn) (string, error) {
	return utils.ReadMessage(connection)
}

// GetServer returns new *Listener
func GetServer(addr string) *Listener {
	log.WithFields(log.Fields{"addr": addr}).Info("Creating server")
	server := &Listener{addr: addr}
	server.init()
	return server
}
