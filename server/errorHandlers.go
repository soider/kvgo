package server

import (
	"net"

	log "github.com/Sirupsen/logrus"
)

func (server *Listener) handleListenError(err error) {
	if err != nil {
		server.Error <- err
	}
}

func (server *Listener) handleAcceptorError(err error) {
	log.WithFields(log.Fields{"error": err.Error()}).Warn("Accepting error")
}

func (server *Listener) handleClientError(err error, connection net.Conn) {
	log.WithFields(log.Fields{"error": err.Error()}).Warn("Client socket error")
	connection.Write([]byte(err.Error()))
	connection.Write([]byte("\n"))
	connection.Close()
}
