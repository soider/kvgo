package main

import (
	"flag"

	"bitbucket.org/eithz/kvgo/application"

	log "github.com/Sirupsen/logrus"
)

func initLogging() {
	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.JSONFormatter{})
}

func init() {
	initLogging()
}

const defaultAddress = "127.0.0.1:10001"
const defaultStorage = "memory"

func main() {
	var ip = flag.String("addr", defaultAddress, "Listen address in <ip>:<port> format")
	var storage = flag.String("storage", defaultStorage, "Storage engine, currently only 'memory' available")
	flag.Parse()
	newApp, err := application.GetApplication(
		ip,
		storage,
	)
	if err != nil {
		log.WithFields(log.Fields{"error": err.Error()}).Fatal("Error starting application")
	}
	newApp.Run()
}
