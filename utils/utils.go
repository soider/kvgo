package utils

import (
	"bufio"
	"io"
	"strings"
)

//ReadMessage read full string from reader
func ReadMessage(connection io.Reader) (string, error) {
	reader := bufio.NewReader(connection)
	line, err := reader.ReadString('\n')
	if err != nil {
		return "", err
	}
	return string(line), nil
}

//TokenizeMessage split message by spaces
func TokenizeMessage(message string) (count int, tokens []string) {
	tokens = strings.Split(strings.Trim(message, "\r\n"), " ")
	count = len(tokens)
	return
}
