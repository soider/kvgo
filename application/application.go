package application

import (
	"errors"
	"os"
	"os/signal"

	"bitbucket.org/eithz/kvgo/server"
	"bitbucket.org/eithz/kvgo/storage"

	log "github.com/Sirupsen/logrus"
)

//ErrorBadStorageType returns on unknown storage type
var ErrorBadStorageType = errors.New("Unknown storage type")

//Application is applications. It holds server and somewhen it would hold storage
type Application struct {
	server *server.Listener
}

// GetApplication returns new application object with specific addr and storage
func GetApplication(addr *string, storage *string) (*Application, error) {
	app := &Application{}
	app.server = server.GetServer(*addr)
	err := app.registerStorage(*storage)
	return app, err
}

//Run starts application. Stops on Done or Error channel inputs.
func (app *Application) Run() {
	app.registerHandlers()
	app.server.Listen()
	app.server.Run()
	log.Info("Server started, begin done and error channel select")
	select {
	case err := <-app.server.Error:
		log.WithFields(log.Fields{"error": err.Error()}).Fatal("Some shit happenen")
	case <-app.server.Done:
		log.Debug("Bye-bye")
	}
}

// Register Interruption signal
func (app *Application) registerHandlers() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		app.server.Stop()
	}()
}

// This would be better as a function, but I pretend to refuse
// of storage module global state and use storage as a field in application
// but in future. Now it's global.
func (app *Application) registerStorage(storageType string) error {
	if storageType != "memory" {
		return ErrorBadStorageType
	}
	storage.RegisterMemoryStorage()
	return nil
}
