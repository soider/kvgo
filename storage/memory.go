package storage

import (
	"time"

	log "github.com/Sirupsen/logrus"
)

//MemoryStorage is naive map based storage. Supports timer based ttl.
// TODO: implement LRU. May be reimplement TTLoverLRU?
type MemoryStorage struct {
	storage          map[string][]byte
	serialChannel    chan ItemDescription
	ttlAbortChannels map[string]chan struct{}
}

// RegisterMemoryStorage registers MemoryStorage as used storage
func RegisterMemoryStorage() {
	memoryStorage := &MemoryStorage{}
	memoryStorage.storage = make(map[string][]byte)
	memoryStorage.serialChannel = make(chan ItemDescription)
	memoryStorage.ttlAbortChannels = make(map[string]chan struct{})
	go memoryStorage.serialWriter()
	storage = memoryStorage
}

// Store stores something to storage
func (s *MemoryStorage) Store(key string, ttl int, data []byte) {
	log.WithFields(log.Fields{"key": key,
		"data": string(data)}).Debug("Try to store something!")
	s.serialChannel <- ItemDescription{Key: key, Data: data, TTL: ttl}
}

// Get gets something from storage
func (s *MemoryStorage) Get(key string) []byte {
	return s.storage[key]
}

func (s *MemoryStorage) expireItem(item ItemDescription, abortChannel chan struct{}) {
	select {
	case <-abortChannel:
		log.WithFields(log.Fields{"key": item.Key}).Debug("Abort TTL expiring")
		return
	case <-time.After(time.Duration(item.TTL) * time.Second):
		log.WithFields(log.Fields{"key": item.Key}).Debug("Item expired")
		item.Data = nil
		s.serialChannel <- item
	}
}

func (s *MemoryStorage) scheduleExpire(item ItemDescription) {
	// If we have any data and ttl for item - schedule clearing goroutine
	if item.TTL != 0 && item.Data != nil {
		abortChannel := s.ttlAbortChannels[item.Key]
		if abortChannel != nil {
			delete(s.ttlAbortChannels, item.Key)
			abortChannel <- struct{}{}
			close(abortChannel)
		}
		abortChannel = make(chan struct{})
		go s.expireItem(item, abortChannel)
		s.ttlAbortChannels[item.Key] = abortChannel
	}
}

func (s *MemoryStorage) serialWriter() {
	log.Debug("Start serial writer")
	for {
		item := <-s.serialChannel
		s.storage[item.Key] = item.Data
		s.scheduleExpire(item)
		log.WithFields(log.Fields{"key": item.Key}).Debug("Stored!!!")
	}
}
