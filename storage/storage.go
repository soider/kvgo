package storage

//Storage is common storage interface, should provide two methods for get and store
type Storage interface {
	Store(key string, ttl int, data []byte)
	Get(key string) []byte
}

var storage Storage

// Set sets value by key for ttl
func Store(key string, ttl int, data []byte) {
	storage.Store(key, ttl, data)
}

// Get gets value by key
func Get(key string) []byte {
	return storage.Get(key)
}

// ItemDescription describes one data unit
type ItemDescription struct {
	Key  string
	Data []byte
	TTL  int
}
