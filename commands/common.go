package commands

import (
	"errors"
	"io"
)

// Invalid command error
var ErrorInvalidCommand = errors.New("Invalid command")

// Invalid arguments error
var ErrorInvalidArgsSize = errors.New("Invalid args size")

// Invalid data size error
var ErrorInvalidDataSize = errors.New("Invalid data size!")

//Command is common command interface
type Command func(io.Reader, string, chan Result, chan error)

//Result is common command result type.
//Close field indicates if connection manager should close client socket.
//Answer stores outgoing message
type Result struct {
	Answer []byte
	Close  bool
}
