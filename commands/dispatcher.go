package commands

import (
	"bitbucket.org/eithz/kvgo/utils"

	log "github.com/Sirupsen/logrus"
)

//Dispatch dispatchs raw command into Command
func Dispatch(command string) Command {
	log.WithFields(log.Fields{"command": command}).Info("Begin parsing")
	_, parts := utils.TokenizeMessage(command)
	var commandFunc Command
	switch parts[0] {
	case "stats":
		commandFunc = StatsCommand
	case "set":
		commandFunc = SetCommand
	case "get":
		commandFunc = GetCommand
	case "quit":
		commandFunc = QuitCommand
	default:
		commandFunc = InvalidCommand
	}
	return commandFunc
}
