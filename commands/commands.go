package commands

import (
	"bytes"
	"io"
	"strconv"

	"bitbucket.org/eithz/kvgo/storage"
	"bitbucket.org/eithz/kvgo/utils"

	log "github.com/Sirupsen/logrus"
)

//InvalidCommand is null command, used by dispatch function in default case
func InvalidCommand(socket io.Reader, _ string, done chan Result, _ chan error) {
	done <- Result{Answer: []byte("Invalid command")}
}

//StatsCommand is currently nul command, but sometime in future it would print some statistics
func StatsCommand(socket io.Reader, _ string, done chan Result, _ chan error) {
	done <- Result{Answer: []byte("Stats output")}
}

//SetCommand stores input value to storage
func SetCommand(socket io.Reader, message string, done chan Result, errChan chan error) {
	count, parts := utils.TokenizeMessage(message)
	if count != 4 {
		errChan <- ErrorInvalidArgsSize
		return
	}
	key, lengthAsString, ttlAsString := parts[1], parts[2], parts[3]
	length, err := strconv.Atoi(lengthAsString)

	if err != nil {
		errChan <- err
		return
	}
	ttl, err := strconv.Atoi(ttlAsString)
	if err != nil {
		errChan <- err
		return
	}

	data, err := utils.ReadMessage(socket)
	if err != nil {
		errChan <- err
		return
	}
	received := len(data)
	if received < length { // TODO: handle \r\n
		errChan <- ErrorInvalidDataSize
		return
	}
	if received > length+2 {
		log.Warning("Have more data than expected")
		data = data[:length]
	}

	storage.Store(key, ttl, []byte(data)[:length])
	done <- Result{Answer: []byte("STORED")}
}

//QuitCommand simply closes current connection
func QuitCommand(socket io.Reader, message string, done chan Result, errChan chan error) {
	done <- Result{Close: true}
}

//GetCommand gets data from storage by key
func GetCommand(socket io.Reader, message string, done chan Result, errChan chan error) {
	count, parts := utils.TokenizeMessage(message)
	if count != 2 {
		errChan <- ErrorInvalidArgsSize
		return
	}
	key := parts[1]
	data := storage.Get(key)
	answer := new(bytes.Buffer)
	if data != nil {
		answer.Write([]byte("VALUE\n"))
		answer.Write(data)
		answer.WriteString("\n")
	}
	answer.WriteString("END")
	done <- Result{Answer: answer.Bytes()}
}
