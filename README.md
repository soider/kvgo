# README #

Simple key-value storage written in golang for learn&training purposes.

### Usage ###
Command `kvgo -addr=127.0.0.1:1000 -storage=memory` will launch storage on specific address with in-memory storage engine (there are no any other engines, however).
Simple connect with telnet and go on.

### Commands ###

* set <key> <size> <ttlseconds>\r\n<data length of size arg>
* get <key>
* quit
* stats (Do nothing, lol)


### Example ###

Server:
```
➜  kvgo git:(master) ✗ ./kvgo -addr="127.0.0.1:9999" -storage="memory"
{"addr":"127.0.0.1:9999","level":"info","msg":"Creating server","time":"2015-09-20T23:09:26+03:00"}
{"level":"info","msg":"Start listeting server","time":"2015-09-20T23:09:26+03:00"}  
{"level":"info","msg":"Server started, begin done and error channel select","time":"2015-09-20T23:09:26+03:00"}  
{"level":"debug","msg":"Start serial writer","time":"2015-09-20T23:09:26+03:00"}  
{"level":"info","msg":"Waiting for connection","time":"2015-09-20T23:09:26+03:00"}
```




Client:
```
➜  ~  telnet 127.0.0.1 9999  
Trying 127.0.0.1...  
Connected to localhost.  
Escape character is '^]'.  
set mykey 4 10  
data  
STORED  
get mykey  
VALUE  
data  
END  
get mykey # Expired on this moment  
END  
quit   
Connection closed by foreign host.
``` 
